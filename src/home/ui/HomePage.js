import Container from "@mui/material/Container";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import {Card, CardMedia} from "@mui/material";

const HomePage = () => {
    return (
      <Container
          component="main"
          maxWidth="lg"
          sx={{
              height: '80vh',
              display: "flex",
              alignContent: "center",
              justifyContent: "center"
          }}
      >

          <Box
              sx={{
                  width: "60%",
                  marginTop: 2,
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "center",
                  justifyContent: "center"
              }}
          >
              <Typography component="h1" variant="h3">
                    Login succeeded!
              </Typography>
             {/* <Box
                  component="img"
                  sx={{
                      height: 360,
                      width: 670,
                  }}
                  alt="IGT"
                  src="public/igt-logo.png"
              />*/}
          </Box>

      </Container>
    );
}

export default HomePage;