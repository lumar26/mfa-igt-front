import { createTheme } from "@mui/material";

// vraca klasicnu temu, nema egzibicija
const theme = createTheme();

export default theme;
