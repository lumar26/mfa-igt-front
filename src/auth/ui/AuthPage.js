import * as React from "react";
import Container from "@mui/material/Container";
import Login from "./login/Login";

const AuthPage = () => {
    return (
        <Container
            component="main"
            maxWidth="lg"
            sx={{
                height: '80vh',
                display: "flex",
                alignContent: "center",
                justifyContent: "center"
            }}
        >
            <Login />
        </Container>
    );
};

export default AuthPage;
