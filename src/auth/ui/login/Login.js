import LockOutlinedIcon from "@mui/icons-material/LockOutlined";
import Avatar from "@mui/material/Avatar";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Grid from "@mui/material/Grid";
import TextField from "@mui/material/TextField";
import Typography from "@mui/material/Typography";
import * as React from "react";
import {useState} from "react";
import axios from "axios";
import {Alert, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, Snackbar} from "@mui/material";
import {useNavigate} from "react-router-dom";

const host = 'localhost:8080'

const Login = () => {
    const [user, setUser] = useState({})
    const [isBalanceAuth, setIsBalanceAuth] = useState(false)
    const [isCodeAuth, setIsCodeAuth] = useState(false)
    const [failedLogin, setFailedLogin] = useState(false)
    const navigate = useNavigate()
    console.log(navigate)

    const handleLogin = (event) => {
        event.preventDefault();
        const loginData = new FormData(event.currentTarget);
        const request = {
            username: loginData.get("email"),
            password: loginData.get("password"),
        }
        axios.post(`http://${host}/auth/login`, {...request})
            .then(() => {
                setUser({...request})
                setIsBalanceAuth(true)
            })
            .catch((error) =>   {
                console.log(error)
                setFailedLogin(true)
            })
    };

    const handleVerify = (event) => {
        event.preventDefault();
        const verifyData = new FormData(event.currentTarget);
        const request = {
            ...user,
            balance: Number(verifyData.get("balance")),
        }
        axios.post(`http://${host}/auth/verify`, {...request})
            .then(() => {
                setIsBalanceAuth(false)
                navigate('/')
            })
            .catch((error) => {
                console.log(error)
                if (error.response.status === 401) {
                    setIsBalanceAuth(false)
                    setIsCodeAuth(true)
                }
            })
    };

    const handleCodeVerify = (event) => {
        event.preventDefault();
        const codeData = new FormData(event.currentTarget);
        const request = {
            ...user,
            code: Number(codeData.get("code")),
        }
        console.log(request);
        axios.post(`http://${host}/auth/login-code`, {...request})
            .then(() => {
                console.log("SUCCESS")
                setIsCodeAuth(false)
                navigate('/')
            })
            .catch((error) => {
                console.log(error)
                setFailedLogin(true)
            })
    };

    function handleClose() {
        setFailedLogin(false)
    }

    return (
        <Box
            sx={{
                width: "50%",
                marginTop: 8,
                display: "flex",
                flexDirection: "column",
                alignItems: "center",
                justifyContent: "center"
            }}
        >
            <Avatar sx={{m: 1, bgcolor: "primary.main"}}>
                <LockOutlinedIcon/>
            </Avatar>
            <Typography component="h1" variant="h5">
                Sign in
            </Typography>
            <Box component="form" noValidate onSubmit={handleLogin} sx={{mt: 3}}>
                <Grid container spacing={2}>
                    <Grid item xs={12}>
                        <TextField
                            required
                            fullWidth
                            id="email"
                            label="Email Address"
                            name="email"
                            autoComplete="email"
                            autoFocus
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            required
                            fullWidth
                            name="password"
                            label="Password"
                            type="password"
                            id="password"
                            autoComplete="password"
                        />
                    </Grid>
                </Grid>
                <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    sx={{mt: 3, mb: 2}}
                >
                    Sign In
                </Button>
            </Box>
            {isBalanceAuth && (
                <Dialog fullWidth open={isBalanceAuth} onClose={() => setIsBalanceAuth(false)}>
                    <Box component="form" noValidate onSubmit={handleVerify}>

                        <DialogTitle>Subscribe</DialogTitle>
                        <DialogContent>
                            <DialogContentText>Please enter your balance:</DialogContentText>
                            <TextField
                                autoFocus
                                margin="dense"
                                id="balance"
                                name="balance"
                                label="Balance"
                                type="number"
                                fullWidth
                                variant="standard"
                            />
                        </DialogContent>
                        <DialogActions>
                            <Button color="primary" variant="outlined" type="submit">Verify</Button>
                        </DialogActions>
                    </Box>
                </Dialog>
            )}
            {isCodeAuth && (
                <Dialog fullWidth open={isCodeAuth} onClose={() => setIsCodeAuth(false)}>
                    <Box component="form" noValidate onSubmit={handleCodeVerify}>

                        <DialogTitle>Subscribe</DialogTitle>
                        <DialogContent>
                            <DialogContentText>Please enter your verification code:</DialogContentText>
                            <TextField
                                autoFocus
                                margin="dense"
                                id="code"
                                name="code"
                                label="Code"
                                type="number"
                                fullWidth
                                variant="standard"
                            />
                        </DialogContent>
                        <DialogActions>
                            <Button color="info" variant="outlined" type="submit">Verify</Button>
                        </DialogActions>
                    </Box>
                </Dialog>
            )}
            {failedLogin && <Snackbar open={true} autoHideDuration={6000} onClose={handleClose}>
                <Alert onClose={handleClose}
                       severity="error"
                       sx={{ width: '100%' }}>
                    Failed to login user!
                </Alert>
            </Snackbar>}
        </Box>
    );
};

export default Login
