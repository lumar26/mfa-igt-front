import Box from "@mui/material/Box";
import { useRoutes } from "react-router-dom";
import AuthPage from "./auth/ui/AuthPage";
import HomePage from "./home/ui/HomePage";

const AppRouter = () => {
    return useRoutes([
        {
            path: "/login",
            element: <AuthPage />,
        },
        {
            path: "/",
            element: <HomePage />,
        },
        { path: "*", element: <Box>NotFound</Box> },
    ]);
};

export default AppRouter;
