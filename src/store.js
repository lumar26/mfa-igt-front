import { configureStore } from "@reduxjs/toolkit";
import userReducer from "./auth/core/user.slice";

const store = configureStore({
    reducer: {
        user: userReducer,
    },
});

export default store;
